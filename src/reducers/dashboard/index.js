const addGenre = (state, genre) => {
    return { ...state, genreList: genre };
};
const addLove = (state, love) => {
    return { ...state, loveListStore: love };
};

export const reducer = (state, action) => {
    
    switch (action.type) {
        case 'ADD_GENRE':
            return addGenre(state, action.genre);
        case 'ADD_LOVE':
            return addLove(state, action.love);
        default:
            return state;
    }
};