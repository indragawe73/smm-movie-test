import React, { useContext, useState, useEffect } from 'react'
import { useHistory } from "react-router-dom";

import './Header.scss';
import { Store } from './../../store'

const Header = () => {
  const history = useHistory()

  const { state } = useContext(Store);
  const [love, setLove] = useState(state.loveListStore);
  const [darkMode, setDarkMode] = useState(false);

  const goHome = () => {
    history.push('/')
  }
  
  const goFavorite = () => {
    history.push('/favorite')
  }

  const handleDarkMode = () => {
    setDarkMode(!darkMode)
    document.documentElement.classList.toggle('dark-mode')
  };

  useEffect(() => {
    setLove(state.loveListStore)
  },[state.loveListStore])
  
  return (
    <div className="header">
      <ul>
        <li onClick={goHome}><h4>Home<span className="border-list">|</span></h4></li>
        <li onClick={goFavorite}><h4>Favorite <span className="total-favorite">({love && <b>{love.length}</b>})</span></h4></li>
      </ul>
      <div className="wrap-switch">
        {darkMode ? 
          <h4 className="label-switch">Light Mode</h4>
          : <h4 className="label-switch">Dark Mode</h4>
        }
        <label className="switch">
          <input type="checkbox" value={darkMode} onChange={handleDarkMode} />
          <div className="slider"></div>
        </label>
      </div>
    </div>
  );
};

export default Header;

