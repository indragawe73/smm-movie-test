import React from 'react'

import './MovieCard.scss';
import Love from './../../components/svg/love'

const imgLink = process.env.REACT_APP_IMG_LINK

const MovieCard = (props) => {

  const listMovie = props.listMovie
  const handleFavorite = props.onClick
  
  return (
    <div className="wrap-movie">

      {listMovie &&
        listMovie.map((list, idx) => (
          <div key={idx} className="wrap-list-movie">
            <div className="wrap-image">
              <div className="hover-favorite">
                <div className="wrap-love" onClick={() => handleFavorite(list)}><Love /></div>
              </div>
              <img className="img-movie" src={`${imgLink}${list.poster_path}`} alt={list.original_title} />
            </div>
            <div><h4 className="title-movie">{list.original_title}</h4></div>
            <div className="wrap-text">
              <p className="text-movie">Rating {list.vote_average}<span className="border-list">|</span></p>
              <p className="text-movie genre">{list.overview}</p>
            </div>
          </div>  
        ))
      }

    </div>
  );
};

export default MovieCard;

