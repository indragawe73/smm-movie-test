import React from "react";
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { HashRouter } from 'react-router-dom';

import Dashboard from './pages/dashboard';
import Favorite from './pages/favorite';
import './App.scss';

const App = () => {
  return (
      <BrowserRouter>
        <Switch>
          <HashRouter>
	          <Route path="/" component={Dashboard} exact />
	          <Route path="/favorite" component={Favorite} exact />
          </HashRouter>
        </Switch>
      </BrowserRouter>
  );
};

export default App;