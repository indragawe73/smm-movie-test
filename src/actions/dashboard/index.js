export const addGenre = (genre, dispatch) => {
    dispatch({
        type: 'ADD_GENRE',
        genre,
    })
};

export const addLove = (love, dispatch) => {
    localStorage.setItem("myLove", JSON.stringify(love));
    dispatch({
        type: 'ADD_LOVE',
        love,
    })
};