import React, { useContext, useEffect, useState } from 'react'
import axiosConfig from '../../utils/axios';

import { addLove } from './../../actions'
import { Store } from './../../store'

import Loading from './../../components/loading'
import Header from './../../components/header'
import MovieCard from './../../components/movieCard'

import './Dashboard.scss';

const myKey = process.env.REACT_APP_API_KEY

const Dashboard = () => {
  const { state, dispatch } = useContext(Store);
  const [isLoading, setIsLoading] = useState(false);
  
  const [isDiscover, setIsDiscover] = useState(false);
  const [listGenre, setListGenre] = useState(null);
  const [showGenre, setShowGenre] = useState(null);
  
  const [listMovie, setListMovie] = useState(null);
  const [page, setPage] = useState(1);
  const [listLove, setListLove] = useState([]);

  const getGenre = 
    () => {
      axiosConfig.get(`/3/genre/movie/list?api_key=${myKey}&language=en-US` )
      .then(response => {
        setListGenre(response.data.genres)
      })
      .catch(error => {
        console.log(error)
        setIsLoading(false)
      })
    }
  
  const getMovie =
    (load) => {
      setIsLoading(true)
      let gen = ''
      if (showGenre){
        gen = showGenre.id
      }
      axiosConfig.get(`/3/discover/movie?api_key=${myKey}&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=${page}&with_genres=${gen}` )
      .then(response => {
        if(load && listMovie) {
          setListMovie(listMovie => [...listMovie, response.data.results].flat())
        } else {
          setListMovie(response.data.results)
        }        
        setTimeout(() => {
          setIsLoading(false)
        }, 1000);
      })
      .catch(error => {
        console.log(error)
        setIsLoading(false)
      })
    }

  const loadMore = 
    (e) => {
      setPage(page + 1)
    }
  
  const handleFavorite = 
    (e) => {
      setIsLoading(true)

      let checkLove = false;
      if(listLove) {
        for(var i = 0; i < listLove.length; i++) {
            if (listLove[i].id === e.id) {
                checkLove = true;
                break;
            }
        }
      }

      if(checkLove) {
        let myLength = null
        for(var j = 0; j < listLove.length; j++) {
            if (listLove[j].id === e.id) {
                myLength = j;
                break;
            }
        }

        listLove.splice(myLength, 1)
        addLove(listLove, dispatch)
        setListLove(listLove)


        setTimeout(() => {
          setIsLoading(false)
        }, 400);
      } else {
        if(listLove) {
          const mirror = [...listLove, e].flat()
          addLove(mirror, dispatch)
          setListLove(listLove => mirror)
        } else {
          setListLove(listLove => e)
        }
        
        setTimeout(() => {
          setIsLoading(false)
        }, 400);
      }
    }

  const showDiscover = 
    (e) => {
      setIsDiscover(true)
    }

  const handleDiscover = 
    (e) => {
      const genreIdx = e.target.id;
      try {
        setPage(1)
        setListMovie(null)
        setShowGenre(listGenre[genreIdx])
      } catch {}
    }

  useEffect(() => {
    setListLove(state.loveListStore)
  },[listLove, state.loveListStore])

  useEffect(() => {
      getMovie(true)
  },[page])

  useEffect(() => {
      getMovie(false)
      setIsDiscover(false)
  },[showGenre])

  useEffect(() => {
    getGenre()
    getMovie()

    const love = JSON.parse(localStorage.getItem("myLove"))
    if(love) {
      setListLove(love)
      addLove(love, dispatch)
    }
  },[dispatch])
  
  return (
    
    <React.Fragment>
      {isLoading && <Loading />}
      <div className="wrapper">
        <Header />
        <div className="main">
          <div className="wrap-discover">
            <div className="discover-label"><h4>Discover :</h4></div>
            <div className="wrap-discover-btn">
                {showGenre ?
                <p className="discover-btn" onClick={showDiscover}>{showGenre.name}</p>
                : <p className="discover-btn" onClick={showDiscover}>all films</p>
                }
                {isDiscover && 
                  <div className="wrap-list-discover">
                    {listGenre &&
                      listGenre.map((list, idx) => (
                        <p key={idx} className="list-discover" id={idx} onClick={handleDiscover}>{list.name}</p>
                      ))
                    }
                    </div>
                }
                <MovieCard listMovie={listMovie} onClick={handleFavorite}/>
            </div>
          </div>
          
        </div>
        <div className="footer">
          <div className="more-btn" onClick={loadMore}>load more {listMovie && <span></span>}</div>
        </div>
      </div>
    </React.Fragment>

  );
};

export default Dashboard;
