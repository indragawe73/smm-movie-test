import React, { useContext, useEffect, useState } from 'react'

import { addLove } from './../../actions'
import { Store } from './../../store'

import Loading from './../../components/loading'
import Header from './../../components/header'
import MovieCard from './../../components/movieCard'

import './Favorite.scss';

const Favorite = () => {

  const { state, dispatch } = useContext(Store);

  const [isLoading, setIsLoading] = useState(false);
  const [love, setLove] = useState(state.loveListStore);

  const handleFavorite = 
    (e) => {
      setIsLoading(true)

      let checkLove = false;
      for(var i = 0; i < love.length; i++) {
          if (love[i].id === e.id) {
              checkLove = true;
              break;
          }
      }

      if(checkLove) {
        let myLength = null
        for(var j = 0; j < love.length; j++) {
            if (love[j].id === e.id) {
                myLength = j;
                break;
            }
        }

        love.splice(myLength, 1)
        addLove(love, dispatch)
        setLove(love)
        
        setTimeout(() => {
          setIsLoading(false)
        }, 800);


      } else {
        const mirror = [...love, e].flat()
        addLove(mirror, dispatch)
        setLove(love => mirror)
        
        setTimeout(() => {
          setIsLoading(false)
        }, 800);
        
      }
    }
  
  useEffect(() => {
    const love = JSON.parse(localStorage.getItem("myLove"))
    setLove(love)
    addLove(love, dispatch)
  },[dispatch])
  
  useEffect(() => {
    setLove(state.loveListStore)
  },[state.loveListStore])

  return (
    
    <React.Fragment>
      {isLoading && <Loading />}
      <div className="wrapper">
        <Header />
        <div className="main">          
          <MovieCard listMovie={love} onClick={handleFavorite}/>
        </div>
      </div>
    </React.Fragment>

  );
};

export default Favorite;
